<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateVulnerabilidadesTable extends Migration
{
    /**
     * Run the migrations.
     */
    public function up()
    {
        Schema::create('vulnerabilidades', function (Blueprint $table) {
            $table->increments('id');
            $table->string('cve');
            $table->string('impacto')->nullable();
            $table->string('solucao')->nullable();
            $table->string('porta')->nullable();
            $table->string('severidade');
            $table->string('exploit')->nullable();
            $table->string('protocolo');
            $table->string('bugstrap')->nullable();
            $table->string('nessus')->nullable();
            $table->text('referencia')->nullable();
            $table->datetime('data_publicacao');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down()
    {
        Schema::dropIfExists('vulnerabilidades');
    }
}
