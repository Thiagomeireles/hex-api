<?php

$finder = Symfony\Component\Finder\Finder::create()
    ->notPath('vendor')
    ->notPath('bootstrap')
    ->notPath('storage')
    ->in(__DIR__)
    ->name('*.php')
    ->notName('*.blade.php')
    ->ignoreDotFiles(true)
    ->ignoreVCS(true);

// return PhpCsFixer\Config::create()
// ->setRules([
// '@PSR2' => true,
// 'array_syntax' => ['syntax' => 'short'],
// 'ordered_imports' => ['sortAlgorithm' => 'alpha'],
// 'no_unused_imports' => true,
// ])
// ->setFinder($finder);
return PhpCsFixer\Config::create()
    ->setRiskyAllowed(true)
    ->setRules(
        [
            '@Symfony' => true,
            'align_multiline_comment' => ['comment_type' => 'all_multiline'],
            'array_indentation' => true,
            'array_syntax' => ['syntax' => 'short'],
            'backtick_to_shell_exec' => true,
            'braces' => true,
            'binary_operator_spaces' => [
                'operators' => [
                    '=' => 'align_single_space_minimal',
                    '=>' => 'align_single_space_minimal',
                    '??' => 'align_single_space_minimal',
                ],
            ],
            'combine_consecutive_issets' => true,
            'combine_consecutive_unsets' => true,
            'comment_to_phpdoc' => true,
            'compact_nullable_typehint' => true,
            'doctrine_annotation_array_assignment' => ['operator' => ':'],
            'doctrine_annotation_braces' => ['syntax' => 'with_braces'],
            'doctrine_annotation_indentation' => true,
            'doctrine_annotation_spaces' => true,
            'ereg_to_preg' => true,
            'error_suppression' => true,
            'explicit_indirect_variable' => true,
            'fully_qualified_strict_types' => true,
            'function_to_constant' => true,
            'increment_style' => ['style' => 'post'],
            'indentation_type' => true,
            'linebreak_after_opening_tag' => true,
            'list_syntax' => ['syntax' => 'short'],
            'logical_operators' => true,
            // 'method_chaining_indentation'                   => true,
            'multiline_comment_opening_closing' => true,
            'multiline_whitespace_before_semicolons' => true,
            'no_multiline_whitespace_around_double_arrow' => true,
            'no_null_property_initialization' => true,
            'no_php4_constructor' => true,
            'no_short_echo_tag' => true,
            'no_unused_imports' => true,
            'no_useless_else' => true,
            'no_useless_return' => true,
            'not_operator_with_successor_space' => true,
            'ordered_imports' => ['sortAlgorithm' => 'length'],
            'phpdoc_add_missing_param_annotation' => true,
            'phpdoc_order' => true,
            'phpdoc_trim_consecutive_blank_line_separation' => true,
            'phpdoc_types_order' => true,
            'self_accessor' => true,
            'simplified_null_return' => true,
        ]
    );
