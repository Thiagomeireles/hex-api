<?php

// dd(App::environment());

// header('Access-Control-Allow-Origin: *');
// header('Access-Control-Allow-Methods: GET, POST, PATCH, PUT, DELETE, OPTIONS');
// header('Access-Control-Allow-Headers: Origin, Content-Type, X-Auth-Token');

use Dingo\Api\Routing\Router;

/** @var Router $api */
$api = app(Router::class);

$api->version('v1', function (Router $api) {
    $api->group([
        'namespace' => 'App\\Api\\V1\\Controllers\\Auth',
        'prefix'    => 'auth',
    ], function (Router $api) {
        // $api->post('signup', 'App\\Api\\V1\\Controllers\\SignUpController@signUp');
        $api->post('register', 'RegisterController@register');
        $api->post('login', 'LoginController@login');

        $api->post('recovery', 'ForgotPasswordController@forgot');
        $api->post('reset', 'ResetPasswordController@beginReset');

        $api->post('logout', 'LogoutController@logout');
        $api->post('refresh', 'RefreshController@refresh');
        $api->get('me', 'UserController@me');
    });

    $api->get('vulnerabilidade/dashboard', 'App\\Api\\V1\\Controllers\\VulnerabilidadeController@dashboard');
    $api->post('upload', 'App\\Api\\V1\\Controllers\\FileUploadController@upload');

    $api->group(['middleware' => 'jwt.auth'], function (Router $api) {
        $api->get('protected', function () {
            return response()->json([
                'message' => 'Access to protected resources granted! You are seeing this text as you provided the token correctly.',
            ]);
        });

        $api->get('refresh', [
            'middleware' => 'jwt.refresh',
            function () {
                return response()->json([
                    'message' => 'By accessing this endpoint, you can refresh your access token at each request. Check out this response headers!',
                ]);
            },
        ]);
    });

    $api->get('test', function () {
        return response()->json([
            'message' => 'Teste para confirmar se a versão 1 da API está up.',
        ]);
    });
});

$api->version('v2', function (Router $api) {
    $api->get('test', function () {
        return response()->json([
            'message' => 'Teste para confirmar se a versão 2 da API está up.',
        ]);
    });
});
