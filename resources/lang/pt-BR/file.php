<?php

return [
    /*
    |--------------------------------------------------------------------------
    | File Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during file upload
    |
     */

    'failed'  => 'Erro ao processar arquivo.',
    'success' => 'Arquivo processado com sucesso!',
];
