<?php

return [
    /*
    |--------------------------------------------------------------------------
    | File Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during file upload
    |
     */

    'failed'  => 'Error uploading the file, please try again or contact our support.',
    'success' => 'File uploaded!',
];
