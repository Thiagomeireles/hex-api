<?php

namespace App\Functional\Api\V1\Controllers;

use Hash;
use App\User;
use Tests\TestCase;
use Illuminate\Foundation\Testing\DatabaseMigrations;

class ForgotPasswordControllerTest extends TestCase
{
    use DatabaseMigrations;

    public function setUp(): void
    {
        parent::setUp();

        $user = new User([
            'name'     => 'Test',
            'email'    => 'test@email.com',
            'password' => Hash::make('123456'),
        ]);

        $user->save();
    }

    public function testReturnSuccessAfterSendingPasswordRecoveryEmailWhenEmailExists()
    {
        $this->post('api/auth/recovery', [
            'email' => 'test@email.com',
        ])->assertJson([
            'status' => trans('passwords.sent'),
        ])->isOk();
    }

    public function testReturnErrorNotFoundWhenUserIsNotFound()
    {
        $this->post('api/auth/recovery', [
            'email' => 'unknown@email.com',
        ])->assertJson([
            'error' => trans('passwords.user'),
        ])->assertStatus(404);
    }

    public function testReturnValidationErrorsWhenDataIsInvalid()
    {
        $this->post('api/auth/recovery', [
            'email' => 'invalidemail.com',
        ])->assertJsonStructure([
            'error',
        ])->assertStatus(422);
    }
}
