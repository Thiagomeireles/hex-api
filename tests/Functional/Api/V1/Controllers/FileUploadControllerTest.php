<?php

namespace App\Functional\Api\V1\Controllers;

use Tests\TestCase;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Storage;

class FileUploadControllerTest extends TestCase
{
    /**
     * @test
     *
     * POST: /api/upload/
     *
     * @param mixed $file
     */
    public function testReturnSuccessWhenFileIsUploadedSuccessfully()
    {
        Storage::fake('local');

        $response = $this->json('post', '/api/upload', [
            'file' => $file = UploadedFile::fake()->create('random.csv', 120),
        ]);

        $response->assertJson([
            'status' => trans('file.success'),
        ])->assertOk();

        // $this->assertEquals('file/' . $file->hashName(), Upload::latest()->first()->file);
        Storage::disk('local')->assertExists('file/'.$file->hashName());

        // $file = $this->getUploadableFile(base_path('tests/files/ativos2.csv'));

        // $response = $this->call('POST', 'api/upload', [], [], ['file' => $file], ['Accept' => 'application/json']);

        // $response->assertJson([
        //     'status' => 'File uploaded!',
        // ])->assertOk();

        // unlink(storage_path('local/files/'.$file));

        // $this->seeInDatabase('subscribers', [
        //     'email' => 'test@test.com'
        // ]);
        // $this->seeInDatabase('users', [
        //     'email' => 'test2@test2.com'
        // ]);
    }

    /**
     * Get uploadable file.
     *
     * @param mixed $file
     *
     * @return UploadedFile
     */
    protected function getUploadableFile($file)
    {
        $dummy = file_get_contents($file);
        file_put_contents(base_path('tests/'.basename($file)), $dummy);
        $path          = base_path('tests/'.basename($file));
        $original_name = 'ativos2.csv';
        $mime_type     = 'text/csv';
        $size          = 111;
        $error         = null;
        $test          = true;
        $file          = new UploadedFile($path, $original_name, $mime_type, $size, $error, $test);

        return $file;
    }
}
