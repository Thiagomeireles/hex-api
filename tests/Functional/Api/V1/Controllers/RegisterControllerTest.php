<?php

namespace App\Functional\Api\V1\Controllers;

use Config;
use Tests\TestCase;
use Illuminate\Foundation\Testing\DatabaseMigrations;

class RegisterControllerTest extends TestCase
{
    use DatabaseMigrations;

    public function testRegisterSuccessfully()
    {
        $this->post('api/auth/register', [
            'name'     => 'Test User',
            'email'    => 'test@email.com',
            'password' => '12345678',
        ])->assertJson([
            'status' => 'ok',
        ])->assertStatus(201);
    }

    public function testRegisterSuccessfullyWithTokenRelease()
    {
        Config::set('hex.sign_up.release_token', true);

        $this->post('api/auth/register', [
            'name'     => 'Test User',
            'email'    => 'test@email.com',
            'password' => '12345678',
            // ])->assertJsonStructure([
            //     'status', 'token'
            // ])->assertJson([
            //     'status' => 'ok'
        ])->assertStatus(201);
    }

    public function testRegisterReturnsValidationError()
    {
        $this->post('api/auth/register', [
            'name'  => 'Test User',
            'email' => 'test@email.com',
        ])->assertJsonStructure([
            'error',
        ])->assertStatus(422);
    }
}
