<?php

namespace App\Api\V1\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;

class FileUploadController extends Controller
{
    public function upload(Request $request)
    {
        $filename = $request->file->getClientOriginalName();

        $request->file('file')->store('file', 'local');

        // if (! $request->file->storeAs('files', \Carbon\Carbon::now()->format('YmdHisu').'_'.$filename)) {
        //     throw new BadRequestHttpException(trans('file.failed'));
        // }

        return response()->json([
            'status' => trans('file.success'),
        ], 200);

        // if (\Storage::disk('local')->put($path.'/'.$filename, \File::get($file))) {
        //     $input['filename'] = $filename;
        //     $input['mime']     = $file->getClientMimeType();
        //     $input['path']     = $path;
        //     $input['size']     = $file->getClientSize();

        //     dd($input);

        //     return response()->json([
        //         'success' => true,
        //         'id'      => $file->id,
        //     ], 200);
        // }
    }
}
