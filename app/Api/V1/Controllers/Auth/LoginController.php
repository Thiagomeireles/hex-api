<?php

namespace App\Api\V1\Controllers\Auth;

use Auth;
use Tymon\JWTAuth\JWTAuth;
use App\Http\Controllers\Controller;
use App\Api\V1\Requests\LoginRequest;
use Tymon\JWTAuth\Exceptions\JWTException;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;

class LoginController extends Controller
{
    /**
     * Log the user in.
     *
     * @param LoginRequest $request validated data
     * @param JWTAuth      $JWTAuth auth guard
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function login(LoginRequest $request, JWTAuth $JWTAuth)
    {
        $credentials = $request->only(['email', 'password']);

        // dd($credentials);

        try {
            $token = Auth::guard()->attempt($credentials);

            if (! $token) {
                throw new AccessDeniedHttpException(trans('auth.failed'));
            }
        } catch (JWTException $e) {
            throw new HttpException(500, trans('auth.error'));
        }

        return response()->json(
            [
                'status'     => 'ok',
                'token'      => $token,
                'expires_in' => (Auth::guard()->factory()->getTTL() * 60),
            ]
        );
    }
}
