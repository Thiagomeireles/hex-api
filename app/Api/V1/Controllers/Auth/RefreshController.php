<?php

namespace App\Api\V1\Controllers\Auth;

use Auth;
use App\Http\Controllers\Controller;

class RefreshController extends Controller
{
    /**
     * Refresh a token.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function refresh()
    {
        try {
            $token = Auth::guard()->refresh();

            return response()->json([
                'status'     => 'ok',
                'token'      => $token,
                'expires_in' => Auth::guard()->factory()->getTTL() * 60,
            ]);
        } catch (\Exception $e) {
            throw new HttpException(500, trans('auth.error'));
        }
    }
}
