<?php

namespace App\Api\V1\Controllers;

use Carbon\Carbon;
use App\Vulnerabilidade;
use Illuminate\Support\Facades\App;
use App\Http\Controllers\Controller;

class VulnerabilidadeController extends Controller
{
    public function dashboard()
    {
        $data = Vulnerabilidade::select('severidade', 'created_at')
            ->whereDate('created_at', '>=', Carbon::now()
            ->subMonths(6))
            ->orderBy('created_at', 'asc')
            ->get()
            ->groupBy(function ($item) {
                return $item->created_at->format('m/y');
                // return $item->created_at->shortMonthName;
            });

        $graph = [
            'Baixo'    => [],
            'Moderado' => [],
            'Alto'     => [],
            'Crítico'  => [],
        ];

        $a = $data->map(function ($vulnerabilidades) use (&$graph) {
            $byMes = $vulnerabilidades->countBy('severidade');

            foreach ($graph as $severidade => $qnt) {
                if ($byMes->has($severidade)) {
                    [
                        $graph[$severidade][] = [
                            'meta'  => $severidade,
                            'value' => $byMes[$severidade],
                        ],
                        // $graph[$severidade]['value'][] = $byMes[$severidade],
                    ];
                } else {
                    $graph[$severidade][] = [
                        'meta'  => $severidade,
                        'value' => 0,
                    ];
                }
            }
        });

        $labels = $data->keys();
        $series = array_values($graph);

        // $a = $data->map(function ($item) {
        //     return $item->countBy('severidade');
        // });

        return response()->json(compact('labels', 'series'));

        /* TESTES */
        // $data = DB::table('vulnerabilidades')
        //           ->selectRaw("severidade, created_at, DATE_FORMAT(created_at, '%m/%Y') AS dataFormatada")
        //           ->whereDate('created_at', '>=', Carbon::now()
        //           ->subMonths(6))
        //           ->groupBy(DB::raw('severidade'))
        //           ->get();

        // $data = Vulnerabilidade::selectRaw("DATE_FORMAT(created_at, '%m/%Y') AS month, severidade, count(*) as qnt")
        //     ->whereDate('created_at', '>=', Carbon::now()
        //     ->subMonths(6))
        //     ->groupBy('severidade', 'month')
        //     ->orderBy('created_at', 'asc')
        //     ->get();
    }
}
