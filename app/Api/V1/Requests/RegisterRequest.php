<?php

namespace App\Api\V1\Requests;

use Dingo\Api\Http\FormRequest;

class RegisterRequest extends FormRequest
{
    public function rules()
    {
        return [
            'name'     => 'required|string|max:255',
            'email'    => 'required|email|max:255|unique:users',
            'password' => 'required|min:8',
        ];
    }

    public function authorize()
    {
        return true;
    }
}
