<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Vulnerabilidade extends Model
{
    use SoftDeletes;

    // protected $dates = ['deleted_at'];

    protected $fillable = [];

    // protected static function boot()
    // {
    //     parent::boot();

    //     static::addGlobalScope(new HoleriteScope());
    // }

    // public static function getHoleritePdfMessage()
    // {
    //     $pathLogoHolerite = addslashes(public_path().DIRECTORY_SEPARATOR.'images'.DIRECTORY_SEPARATOR.'logo_holerite.jpg');

    //     // return Holerite::with('arquivo')->find(1);

    //     return self::join('holerite_tipos', 'holerites.holerite_tipo_id', '=', 'holerite_tipos.id')
    //         ->leftJoin('bancos', 'bancos.id', '=', 'holerites.banco')
    //         ->join('usuarios', 'usuarios.id', '=', 'holerites.usuario_id')
    //         ->join('arquivos', 'arquivos.id', '=', 'holerites.arquivo_id')
    //         ->join('empresas', 'empresas.id', '=', 'arquivos.empresa_id')
    //         ->join('grupos', 'grupos.id', '=', 'empresas.grupo_id')
    //         ->select([
    //             'holerites.id',
    //             'holerites.matricula',
    //             'holerite_tipos.descricao as holerite_tipo',
    //             'holerites.cpf',
    //             'holerites.nome',
    //             'holerites.cargo',
    //             'holerites.total_proventos',
    //             'holerites.total_descontos',
    //             'holerites.total_liquido',
    //             'holerites.banco as banco_codigo',
    //             'bancos.nome as banco_nome',
    //             'holerites.agencia',
    //             'holerites.agencia_digito',
    //             'holerites.conta',
    //             'holerites.conta_digito',
    //             'holerites.referencia_ano',
    //             'holerites.referencia_mes',
    //             'holerites.data_pagamento',
    //             'holerites.empresa_cnpj',
    //             'holerites.empresa_nome',
    //             'holerites.texto',
    //         ])
    //         ->addSelect(
    //                    \DB::raw(
    //                        "
    //                           IFNULL(grupos.caminho_img, '".$pathLogoHolerite."') as logotipo,
    //                           CONCAT(LPAD(holerites.referencia_mes, 2, 0), '/', holerites.referencia_ano) as referencia,
    //                           date_format(holerites.created_at, '%d/%m/%Y') as data_inclusao,
    //                           date_format(holerites.created_at, '%H:%i:%s') as hora_inclusao,
    //                           UNIX_TIMESTAMP(holerites.created_at) as timestamp"
    //                        )
    //                )
    //             //    ->toSql();
    //         ->get();
    // }

    // public function usuario()
    // {
    //     return $this->belongsTo('App\Entities\Usuario');
    // }

    // public function tipo()
    // {
    //     return $this->hasOne('App\Entities\HoleriteTipo', 'id', 'holerite_tipo_id');
    // }

    // public function bancos()
    // {
    //     return $this->hasOne('App\Entities\Banco', 'id', 'banco');
    // }

    // public function empresa()
    // {
    //     return $this->hasManyThrough(
    //         'App\Entities\Empresa',
    //         'App\Entities\Arquivo',
    //         'id',
    //         'id',
    //         'arquivo_id',
    //         'empresa_id'
    //     );
    // }

    // public function arquivo()
    // {
    //     // return $this->hasOne('App\Entities\Arquivo', 'arquivos')->withTimestamps();
    //     return $this->belongsTo('App\Entities\Arquivo');
    // }
}
